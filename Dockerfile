FROM alpine as base
RUN apk update

FROM base as builder

RUN apk add \
		git \
		build-base && \
	apk add \
		libxslt \
		ncurses-dev \
		libidn-dev

RUN git clone -b master https://github.com/neomutt/neomutt
RUN cd neomutt && \
	./configure --disable-doc --disable-nls && \
	make && \
	make install

FROM base as neomutt
COPY --from=builder /usr/bin/neomutt /usr/bin/neomutt
#COPY --from=builder /usr/share/neomutt /usr/share/neomutt
#COPY --from=builder /usr/share/doc/neomutt /usr/share/doc/neomutt
#COPY --from=builder /usr/share/man/
COPY --from=builder /usr/libexec/neomutt /usr/libexec/neomutt
COPY --from=builder /usr/lib/libidn* /usr/lib
COPY --from=builder /usr/lib/libncurses* /usr/lib

RUN apk add xterm

# Set a group and user
RUN addgroup -S neomutt && adduser -S neomutt -G neomutt mail

USER neomutt 
WORKDIR /home/neomutt

RUN mkdir Mail

CMD [ "/usr/bin/neomutt" ]
